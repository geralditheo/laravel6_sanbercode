<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', 'HomeController@index');

Route::get('/register', 'RegisterController@index' );

Route::post('/register', 'RegisterController@register');

Route::post('/welcome', function(Request $request){

    return view('/welcome', ["firstname" => $request['firtname'], "lastname" => $request['lastname']]);
});

Route::get('master', function(){
    return view('layouts.master');
});

Route::get('/table', function(){
    
    return view('layouts.sections.table');
});

Route::get('/data-table', function(){
    return view('layouts.sections.data-tables');
});


// Cast

Route::get('/cast', 'CastController@index' );
Route::get('/cast/create', 'CastController@create' );
Route::post('/cast', 'CastController@store');

Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('cast/{id}', 'CastController@update');

Route::delete('/cast/{id}','CastController@destroy' );