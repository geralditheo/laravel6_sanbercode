<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<html>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST"  >
        @csrf

        <label>First Name</label>
        <br />
        <input type="text" name="firtname" />
        <br />

        <label>Last Name</label>
        <br />
        <input type="text" name="lastname" />
        <br />

        <label>Gender</label>
        <br />
        <input type="radio" name="gender" value="male" /> Male
        <br />
        <input type="radio" name="gender" value="female" /> Female
        <br />
        <input type="radio" name="gender" value="other" /> Other
        <br />

        <label>Nationality</label>
        <br />
        <select name="nasional" >
            <option>Indonesian</option>
            <option>America</option>
            <option>Englist</option>
            <option>Other</option>
        </select>
        <br />

        <label>Language Spoken</label>
        <br />
        <input type="checkbox" value="indonesia" name="bahasa" /> Bahasa Indonesia
        <br />
        <input type="checkbox" value="english" name="bahasa" /> English
        <br />
        <input type="checkbox" value="other" name="bahasa" /> Other
        <br />

        <label>Bio</label>
        <br />
        <textarea placeholder="Isi" name="biodata" cols="30" rows="15" > </textarea>
        <br />

        
        <input type="submit">
        
    </form>
</body>
</html>