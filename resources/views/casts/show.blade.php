@extends('layouts.master');

@section('content')
    <div class="mx-3 my-3" >
        <h1> {{ $caster->nama  }} </h1>
        <h4> {{$caster->bio }} </h4>
        <p> {{ $caster->umur }} </p>
    </div>
@endsection