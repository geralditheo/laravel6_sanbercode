@extends('layouts.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Insert New Caster</h3>
        </div>

        <form action="/cast" method="POST" >
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="nama" value=" {{ old('nama', '') }} " id="title" >
                    
                    @error('nama')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="body">Biodata</label>
                    <input type="text" name="bio" class="form-control" id="body" value=" {{ old('bio', '') }} "  >

                    @error('bio')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="body">umur</label>
                    <input type="number" name="umur" class="form-control" id="body" value=" {{ old('umur', '') }} "  >

                    @error('umur')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>
            </div>
            
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
@endsection