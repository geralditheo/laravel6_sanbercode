@extends('layouts.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Insert New Caster</h3>
        </div>

        <form action="/cast/{{ $caster->id }}" method="POST" >
            @csrf
            @method('PUT')

            <div class="card-body">
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="nama" value=" {{ old('nama', $caster->nama ) }} " id="title" >
                    
                    @error('nama')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="body">Biodata</label>
                    <input type="text" name="bio" class="form-control" id="body" value=" {{ old('bio', $caster->bio) }} "  >

                    @error('bio')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="body">umur</label>
                    <input type="text"  name="umur" class="form-control" id="body" value=" {{ (int)$caster->umur }} "  >

                    @error('umur')
                        <div class="alert alert-danger" > {{$message}} </div>
                    @enderror
                </div>
            </div>
            
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
@endsection