<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casters = DB::table('casts')->get();

        // dd($casters->all());

        return view('casts.index', compact('casters') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'umur' => 'required',
        ]);


        $store = DB::table('casts')->insert([
            'nama' => $request['nama'],
            'bio' => $request['bio'],
            'umur' => intval($request['umur']) 
        ]);

        return redirect('/cast')->with('success', "Cast Berhasil Di Store");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $caster = DB::table('casts')->where('id', $id)->first();

        return view('/casts.show', compact('caster'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caster = DB::table('casts')->where('id', $id)->first();

        return view('/casts.edit', compact('caster'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'umur' => 'required',
        ]);

        $update = DB::table('casts')->where('id', $id)->update([
            'nama' => $request['nama'],
            'bio' => $request['bio'],
            'umur' => intval($request['umur']) 
        ]);

        return redirect('/cast')->with('success', 'Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = DB::table('casts')->where('id', $id)->delete();

        return redirect('/cast')->with('success', 'Berhasil Di Delete');
    }
}
