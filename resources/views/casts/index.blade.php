@extends('layouts.master');


@section('content')
    <div class="mx-3 my-3" >
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
            </div>
             
            <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" > 
                    {{ session('success') }} 
                </div>
            @endif

            <a href="/cast/create" class="btn btn-info mb-3 " >Create New Caster</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Biodata</th>
                    <th>Umur</th>
                    <th style="width: 40px">Action</th>
                    </tr>
                </thead>
            <tbody>
                

                @forelse ($casters as $key => $caster)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{$caster->nama}} </td>
                    <td> {{ $caster->bio }} </td>
                    <td> {{ $caster->umur }} </td>
                    <td style="display: flex" >
                        <a href="/cast/{{$caster->id}}" class="btn btn-info btn-sm" > show </a>
                        <a href="/cast/{{$caster->id}}/edit" class="btn btn-default btn-sm" > edit </a>
                        <form action="/cast/{{$caster->id}}" method="POST" >
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm"  />
                        </form>
                    </td>
                </tr>

                @empty
                    <tr>
                        <td colspan="4" align="center"  > No Posts </td>
                    </tr>
                
                @endforelse
              
            </tbody>
            </table>
            </div>
           
            </div>
    </div>
@endsection